<h2> Hi, I'm Baptiste Gislot! <img src="https://media.giphy.com/media/26xBwdIuRJiAIqHwA/giphy.gif" width="25"></h2>
<img align='right' src="https://camo.githubusercontent.com/992babdffd8c74a1502de375fbdf7e4d54773242/68747470733a2f2f6d656469612e67697068792e636f6d2f6d656469612f53576f536b4e36447854737a71494b4571762f67697068792e676966" width="270">
<p><em>💻 Junior Full Stack Developer 🚀</em></p>

[![Website: baptistegislot](https://img.shields.io/badge/-baptistegislot.fr-ff69b4?style=flat-square&logo=brave&logoColor=white&link=https://www.baptistegislot.fr/)](https://www.baptistegislot.fr)
[![Linkedin: baptistegislot](https://img.shields.io/badge/-baptistegislot-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/baptistegislot/)](https://www.linkedin.com/in/baptistegislot/)
[![GitHub: KobeSan](https://img.shields.io/github/followers/KobeSan?label=Follow%20Me%20%21&style=social)](https://github.com/KobeSan)


### <img src="https://user-images.githubusercontent.com/5713670/87202985-820dcb80-c2b6-11ea-9f56-7ec461c497c3.gif" width="50"> A little more about me...  

```javascript
const baptiste = {
  code: [Javascript(ES6), HTML5, CSS3, SQL],
  tools: [React, Redux, Node, Mocha, Phaser, Docker, Debian, Nginx],
  architecture: ["microservices", "test-driven", "design system pattern"],
  hobbies: {
                        running: "Marathon / Trail...",
                        cooking: "Italian / Asian... ",
                        music: "Hard Rock / Heavy Metal / Rock...",
                        travel: "USA / Reunion Island / England / Portugal...",
                        coding: "Obviously..."
                      }
};
```

<img src="https://camo.githubusercontent.com/9ade64e11a552681b4085259e9ff127659b1be6a/68747470733a2f2f6d656469612e67697068792e636f6d2f6d656469612f524b354b443655635570417439327a5a76742f67697068792e676966" width="50"> <em><b>I ❤️ coding and keep learning everyday !</b> I am currently looking for a job, so if you want to say <b>hi, I'll be happy to meet you !</b> </em>